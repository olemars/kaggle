# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import OneHotEncoder, LabelEncoder, Imputer
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier, BaggingClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import accuracy_score
from sklearn import svm
import xgboost as xgb

train = pd.read_csv("train.csv")
test = pd.read_csv("test.csv")



#infer missing
train["Embarked"] = train["Embarked"].fillna("Unknown")
train["Cabin"] = train["Cabin"].fillna("Unknown")
train["Age"] = train["Age"].fillna(train["Age"].mean())

test["Fare"] = test["Fare"].fillna(train["Fare"].mean())
test["Cabin"] = test["Cabin"].fillna("Unknown")
test["Age"] = test["Age"].fillna(train["Age"].mean())

#categorize gender
#categorize embarked
#categorize class
def convert_category(dataset, feature):
    le = LabelEncoder()
    oh = OneHotEncoder()
    int_coding = le.fit_transform(dataset[feature]).reshape(-1,1)
    onehot_coding = oh.fit_transform(int_coding).toarray()
    classes = list(le.classes_)
    featurenames = [feature + "_" + str(c) for c in classes]
    df = pd.DataFrame(onehot_coding, columns=featurenames)
    return dataset.join(df).drop(labels=feature, axis=1)
    
    
train = convert_category(train, "Embarked")
train = convert_category(train, "Sex")
train = convert_category(train, "Pclass")

test = convert_category(test, "Embarked")
test = convert_category(test, "Sex")
test = convert_category(test, "Pclass")

#maybe bin fare/age
def convert_buckets(dataset, feature, buckets):
    bucketnames = [feature + "_" + str(bucket) for bucket in buckets[:-1]]
    categorized = pd.cut(dataset[feature], buckets, labels=bucketnames)
    dataset[feature] = categorized
    return convert_category(dataset, feature)

#train = convert_buckets(train, "Fare", [-1, 0, 8, 15, 31, 1000])
#train = convert_buckets(train, "Age", [-1, 0, 5, 12, 18, 25, 35, 60, 110])
#
#test = convert_buckets(test, "Fare", [-1, 0, 8, 15, 31, 1000])
#test = convert_buckets(test, "Age", [-1, 0, 5, 12, 18, 25, 35, 60, 110])

#maybe merge sibso+parch

#drop unneeded features
labels = train["Survived"]
sub_ids = test["PassengerId"]
train = train.drop(labels=["PassengerId", "Survived", "Cabin", "Ticket", "Name", "Embarked_Unknown"], axis=1)
test = test.drop(labels=["PassengerId", "Cabin", "Ticket", "Name"], axis=1)

#split into train + validation
train_x, val_x, train_y, val_y = train_test_split(train, labels, test_size=0.2)

#setup and train model
my_model = RandomForestClassifier(n_estimators=256, max_depth=8)
#my_model = xgb.XGBClassifier(max_depth=3, n_estimators=500, learning_rate=0.05)
my_model.fit(train_x, train_y)
pred = my_model.predict(val_x)
print("acc: " + str(accuracy_score(val_y, pred)))

result = pd.Series(my_model.predict(test), name="Survived")
out = pd.DataFrame(sub_ids)
out = out.join(result)
out.to_csv("submission.csv", index=False)
