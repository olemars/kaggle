# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
from sklearn.preprocessing import OneHotEncoder, LabelEncoder, Imputer
import keras
from keras.models import Sequential
from keras.layers import Dense, Activation
from keras.utils import to_categorical


train = pd.read_csv("train.csv")
test = pd.read_csv("test.csv")


#infer missing
train["Embarked"] = train["Embarked"].fillna("Unknown")
train["Cabin"] = train["Cabin"].fillna("Unknown")
train["Age"] = train["Age"].fillna(train["Age"].mean())

test["Fare"] = test["Fare"].fillna(train["Fare"].mean())
test["Cabin"] = test["Cabin"].fillna("Unknown")
test["Age"] = test["Age"].fillna(train["Age"].mean())

#categorize gender
#categorize embarked
def convert_category(dataset, feature):
    le = LabelEncoder()
    oh = OneHotEncoder()
    int_coding = le.fit_transform(dataset[feature]).reshape(-1,1)
    onehot_coding = oh.fit_transform(int_coding).toarray()
    classes = list(le.classes_)
    featurenames = [feature + "_" + c for c in classes]
    df = pd.DataFrame(onehot_coding, columns=featurenames)
    return dataset.join(df).drop(labels=feature, axis=1)
    
    
train = convert_category(train, "Embarked")
train = convert_category(train, "Sex")

test = convert_category(test, "Embarked")
test = convert_category(test, "Sex")

#maybe bin fare
#maybe merge sibso+parch

#prune unneeded features
labels = to_categorical(train["Survived"])
train = train.drop(labels=["Survived", "Cabin", "Ticket", "Name", "Embarked_Unknown"], axis=1)
test = test.drop(labels=["Cabin", "Ticket", "Name"], axis=1)

my_model = Sequential()
my_model.add(Dense(2, input_dim=11))
my_model.add(Dense(32))
my_model.add(Activation('relu'))
my_model.add(Dense(2))
my_model.compile(optimizer='adagrad', loss='categorical_crossentropy', metrics=["accuracy"])
my_model.fit(train, labels, validation_split=0.2)