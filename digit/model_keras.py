# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
import keras
from keras.models import Sequential
from keras.layers import Conv2D, Flatten, Dense, Activation
from keras.utils import to_categorical

train = pd.read_csv("train.csv")
test = pd.read_csv("test.csv")


img_rows, img_cols = 28, 28
num_classes = 10

def data_prep(raw):
    if raw.columns.contains("label"):
        out_y = keras.utils.to_categorical(raw.label, num_classes)
        start_col = 1
    else:
        out_y = None
        start_col = 0

    num_images = raw.shape[0]
    x_as_array = raw.values[:,start_col:]
    x_shaped_array = x_as_array.reshape(num_images, img_rows, img_cols, 1)
    out_x = x_shaped_array / 255
    return out_x, out_y


x, y = data_prep(train)
testx,_ = data_prep(test)

model = Sequential()
model.add(Conv2D(20, kernel_size=(3, 3),
                 activation='relu',
                 input_shape=(img_rows, img_cols, 1)))
model.add(Conv2D(20, kernel_size=(3, 3), activation='relu'))
model.add(Flatten())
model.add(Dense(128, activation='relu'))
model.add(Dense(num_classes, activation='softmax'))

model.compile(loss=keras.losses.categorical_crossentropy,
              optimizer='adam',
              metrics=['accuracy'])
model.fit(x, y,
          batch_size=128,
          epochs=2,
          validation_split = 0.2)

pred = pd.DataFrame(model.predict(testx))
result = pred.apply(pd.Series.idxmax, axis=1)
result.name = "label"
result.index += 1
result.to_csv("submission.csv", header=True, index_label="ImageId")
